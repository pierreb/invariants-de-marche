restart;

# crat.mpl est tout le code qui définit CalcInvRat
read("crat.mpl"):

#%%

# On calcule une première paire d’invariants rationnels
ST:={[-1,-1],[1,-1],[2,1],[0,1]}:
K := 1 - t*subs(add(x^k[1]*y^k[2],k=ST)):
I1,J1:=CalcInvRat(ST,x):
expand(I1); expand(J1);

# On reprend le découplage de xy comme F+G pour construire une deuxième paire d’invariants
F:=x/(t*(x^2+1)): G:=-1/y:
eqQ:= (-x*y*K*Q(x,y) + x*y-t*Q(x,0)*(1+x^2)-t*Q(0,y)+t*Q(0,0))/(x*y);

I2:=F-t*Q(x,0)*(1+x^2)+t*Q(0,0); J2:=t*Q(0,y)-G;

# on élimine les pôles progressivement
series(I1,x,4); series(J1,y,4);
series(I2,x,4); series(J2,y,4);
subs(x=0,I2);

I3:=simplify(I2*I1);
J3:=simplify(J2*J1);

J4:=J3+J2^3-2*t*Q(0,0)*J2^2-(2*t*D[2](Q)(0,0)-t^2*Q(0,0)^2)*J2;
I4:=I3+I2^3-2*t*Q(0,0)*I2^2-(2*t*D[2](Q)(0,0)-t^2*Q(0,0)^2)*I2;

J4:=simplify(J4);
I4:=simplify(I4);

# conviennent
series((I4-J4)/K,x,1);
series((I4-J4)/K,y,1);

# les deux sont sans pôles en 0
J4_0:=subs(y=0,series(J4,y));
I4_0:=subs(x=0,series(I4,x));


#%%

## première équation à une variable catalytique :

p1:=numer(I4)-denom(I4)*A(t):
p1:=subs({Q(0,0)=F1(t),D[2](Q)(0,0)=F2(t)},p1);

## bien formée:

# F1(t) = Q(0,0)
eq:=subs(x=0,p1);
F1_Q:=solve(eq,F1(t));

# A(t) = f(D[1](Q)(0,0))
eq:=subs(F1(t)=F1_Q,subs(x=0,diff(p1,x)));
A_Q:=solve(eq,A(t));

# F2(t) = f(F1,Q',Q'')
eq:=subs(F1(t)=F1_Q,A(t)=A_Q,subs(x=0,diff(diff(p1,x),x)));
F2_Q:=normal(solve(eq,F2(t)));

# On remplace maintenant par les bonnes dérivées (plus pratique pour prouver l’algébricité)

pol:=numer(subs(Q(x,0)=x1,Q(0,0)=x2,D[1](Q)(0,0)=x3,D[1,1](Q)(0,0)=x4,simplify(subs(F1(t)=F1_Q,F2(t)=F2_Q,A(t)=A_Q,p1))));

# on réécrit sous forme canonique
d:=(x1-x2-x*x3-x^2*x4/2)/x^3;
c:=(x1-x2-x*x3)/x^2;
b:=(x1-x2)/x;
a:=x1;

pol:
simplify(subs(x4=solve(d-D3,x4),%)):
simplify(subs(x3=solve(c-D2,x3),%)):
simplify(subs(x2=solve(b-D1,x2),%)):
collect(%,t);
simplify(%/x^3/4);


# bilan : x1 = Q(x,0) est algébrique, et Q(0,y) aussi par voie de conséquence


#%%
