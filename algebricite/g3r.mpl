restart;


read("crat.mpl"):

#%%

ST:={[-1,-1],[0,1],[1,0],[1,-1],[2,1]}:
K := 1 - t*subs(add(x^k[1]*y^k[2],k=ST)):

# Première paire d’invariants rationnels non-triviaux
I1,J1:=CalcInvRat(ST,x);
expand(I1); expand(J1);

# Deuxième paire d’invariants

# on réutilise le découplage de xy = F+G
F := -1/4*(3*t*x^2-t-4*x)/(x^2+1)/t;
G := -1/4*(y+4)/y;

eqQ:=1-t*(1/(x*y)+x/y)*Q(x,0)-t/(x*y)*Q(0,y)+t/(x*y)*Q(0,0)-K*Q(x,y);

#Qser:=proc(n) option remember:
#if n=0 then 1 else normal( 
#    series( 
#    subs( 
#        Q(x,y)=Qser(n-1), 
#        Q(0,0)=subs(x=0,y=0,Qser(n-1)), 
#        Q(x,0)=subs(y=0,Qser(n-1)), 
#        Q(0,y)=subs(x=0,Qser(n-1)), 
#        eqQ+Q(x,y))
#    ,t,n+1)
#    ): fi: end:
#
#Q:=map(expand,Qser(4));
#
#Qx0:=subs(y=0,Q);
#Q0y:=subs(x=0,Q);
#Q00:=subs({x=0,y=0},Q);


# Cela produit une deuxième paire d’invariants
I2:=F-t*Q(x,0)*(1+x^2)+t*Q(0,0); J2:=t*Q(0,y)-G;
series(I1,x,4); series(J1,y,4);
series(I2,x,4); series(J2,y,4);



# On produit une autre paire d’invariants sans pôles
I3:=simplify((I2-1/4)*I1);
J3:=simplify((J2-1/4)*J1);
series(I3,x,4); series(J3,y,4);
series(I2,x,4); series(J2,y,4);

I2:=I2-subs(y=0,diff(normal(y*J2),y));
J2:=J2-subs(y=0,diff(normal(y*J2),y));


#on continue ce processus d’élimination une fois

I4:=I3+I2^3;
J4:=J3+J2^3;
series(J4,y,4);

#encore une fois
I5:=normal(I4-subs(y=0,normal(y^2*J4))*I2^2);
J5:=normal(J4-subs(y=0,normal(y^2*J4))*J2^2);
series(J5,y,4);

# une dernière fois
I6:=normal(I5-(-t^2*Q(0,0)+2*t^2*D[2](Q)(0,0))/t*I2);
J6:=normal(J5-(-t^2*Q(0,0)+2*t^2*D[2](Q)(0,0))/t*J2);
series(I6,x,3);
series(J6,y,3);

# Cette paire d’invariants satisfait les conditions du lemme des invariants
series((I6-J6)/K,x,1);
series((I6-J6)/K,y,1);
J6_0:=subs(y=0,series(J6,y));
I6_0:=subs(x=0,series(I6,x));


#%%

# On cherche maintenant à mettre l’une des équations à une variable catalytique
# sous la forme canonique du papier de Mireille Bousquet-Mélou et Arnaud Jehanne

# On choisit la première première équation à une variable catalytique :

p1:=numer(I6)-denom(I6)*A(t):
p1:=subs({Q(0,0)=F1(t),D[2](Q)(0,0)=F2(t)},p1);

## Cette équation est bien formée (les petits fonctions sont entièrement déterminées par Q)

# F1(t) = Q(0,0)
eq:=subs(x=0,p1);
F1_Q:=solve(eq,F1(t));

# A(t) = f(D[1](Q)(0,0))
eq:=subs(F1(t)=F1_Q,subs(x=0,diff(p1,x)));
A_Q:=solve(eq,A(t));

# F2(t) = f(F1,Q',Q'')
eq:=subs(F1(t)=F1_Q,A(t)=A_Q,subs(x=0,diff(diff(p1,x),x)));
F2_Q:=normal(solve(eq,F2(t)));

# On remplace maintenant par les dérivées partielles de Q(x,0) (plus pratique pour prouver l’algébricité)

pol:=numer(subs(Q(x,0)=x1,Q(0,0)=x2,D[1](Q)(0,0)=x3,D[1,1](Q)(0,0)=x4,simplify(subs(F1(t)=F1_Q,A(t)=A_Q,F2(t)=F2_Q,p1))));

# On réécrit alors l’équation sous forme canonique
d:=(x1-x2-x*x3-x^2*x4/2)/x^3;
c:=(x1-x2-x*x3)/x^2;
b:=(x1-x2)/x;
a:=x1;

pol:
%:=%/(4*x^3):
simplify(subs(x4=solve(d-D3,x4),%)):
simplify(subs(x3=solve(c-D2,x3),%)):
simplify(subs(x2=solve(b-D1,x2),%)):
collect(%,t);


# Bilan : x1 = Q(x,0) est algébrique, et Q(0,y) aussi par voie de conséquence

#%%
