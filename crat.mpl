# This code finds the minimal polynomials of the orbit sum elements
# It returns two lists of irreducible polynomials: Ps (in x,y,X) and 
# Qs (in x,y,Y) such that for every root x' of a polynomial p in Ps 
# there exists a root y' of some polynomial q in Qs such that (x',y')
# is in the orbit (and vice-versa, for the roots of the elements in Qs)

PQList := proc(ST) 
  local char, charS,Ps,Qs,added,p,q,newQ,newP,newQtemp,newPtemp,k;
  
  char := subs(add(x^k[1]*y^k[2],k=ST)):
  charS := numer(simplify((subs(x=X,y=Y,char)-char))):
  Ps := {X-x}:
  Qs := {Y-y}:
  added := true:

  while max(map(degree,Ps,X)) < 100 and max(map(degree,Qs,Y)) < 100 and max(nops(Ps),nops(Qs)) < 100 and added do
    added := false:
    newQ := {}:
    for p in Ps do
      newQtemp := factor(primpart(resultant(charS,p,X),Y));
      newQ := newQ union {seq(k[1],k=sqrfree(newQtemp)[2])};
    od;
    newP := {}:
    for q in Qs do
      newPtemp := factor(primpart(resultant(charS,q,Y),X));
      newP := newP union {seq(k[1],k=sqrfree(newPtemp)[2])};
    od;

    added := `not`((newP union newQ) subset (Ps union Qs)):
    Ps := Ps union newP:
    Qs := Qs union newQ:
  od:

 if added = false then return Ps,Qs, charS;
 else return -1,-1;
 fi:
end:

#%%

# maintenant, on veut aller plus loin et trouver directement une
# paire d’invariants rationnels

with(LinearAlgebra):
with(PolynomialTools):

# Évalue un polynôme en une matrice carrée par la méthode
# de Horner
horner := proc(p,m,x)
    local z, cc;

    cc := CoefficientList(p,x,termorder=reverse);
    z := ZeroMatrix(Dimension(m));

    #return seq[fold=(((u,v)->m.u+v), z)](cc);
    return foldl((u,v)->m.u+v, z, op(cc));
end:


# Calcule la somme d’une fraction rationnelle évaluée sur chaque racine
# d’un polynôme de Laurent.
# En substance, Σ h(x_i) = Tr (h (C_p))
# on le fait en deux étapes
# 1) on normalise h par rapport à p pour en faire un polynôme
# (pour cela, on utilise l’algorithme d’euclide étendu)
# 2) on l’évalue en la matrice compagnon m (par méthode de Horner
# par exemple) puis on calcule sa trace
sumRoots := proc(h,p,x)
    local m, np, nh, n, d;

    # on met le polynôme de laurent sous forme polynômiale unitaire
    np := numer(p);
    np := np / lcoeff(np, x);

    # on calcule h mod np
    nh := normal(h);
    n := numer(nh);
    d := denom(nh);
    gcdex(np,d,n,x,'s','q');

    # on évalue la matrice compagnon de np en ce polynôme
    m := CompanionMatrix(np,x);
    return Trace(horner(q,m,x));
end:

#%%

# par exemple, on peut avoir la série génératrice des fonctions
# symétriques des puissances pour le polynôme général de
# degré 3 
f := sumRoots(1/(1-x*y),x^3-a*x^2+b*x-c,x):
series(f,y);

#%%

# Cette procédure produit à partir d’une orbite finie
# une paire d’invariants rationnels, paramétrisés
# par une fraction rationnelle H en x et y
CalcInvRat := proc(ST,h) 
  local k, char, ker, mu1, mu2, a, b, I1, J1, Ps, Qs, _:
  
  char := subs(add(x^k[1]*y^k[2],k=ST)):
  ker := numer(normal(char-1/t)):

  Ps,Qs,_ := PQList(ST):
  mu1 := mul(Ps): mu2 := mul(Qs):

  # premier invariant
  a := sumRoots(h, ker, y):
  b := sumRoots(subs(x=X,a), mu1, X):
  I1 := sumRoots(b, ker, y) / (degree(ker, y))/2:

  # deuxième invariant
  a := sumRoots(h, ker, x):
  b := sumRoots(subs(y=Y,a), mu2, Y):
  J1 := sumRoots(b, ker, x) / (degree(ker, x))/2:

  return simplify(I1), simplify(J1);
end:

#%%

# Exemples :

#%% cas 1 non retourné
ST:={[1,1],[-1,1],[-2,-1],[0,-1]};

K := 1 - t*subs(add(x^k[1]*y^k[2],k=ST)):
I1,J1:=CalcInvRat(ST,x);
simplify(normal((I1-J1)/K));


#%%

S:=(1-K)/t;

simplify(sumRoots(X*y, subs(x=X,S)-S, X));
simplify(sumRoots(X/(x*y), subs({x=X,y=1/(x*y)},S)-subs(y=1/(x*y),S),X));

#%% G2r
ST:={[-1,-1],[1,-1],[2,1],[0,1]};
S:=subs(add(x^k[1]*y^k[2],k=ST));
K := numer(1 - t*S):
I1,J1:=CalcInvRat(ST,x);
simplify(normal((I1-J1)/K));

#%% G2
ST:={[1,1],[0,-1],[-1,0],[-1,1],[-2,-1]}:
S:=subs(add(x^k[1]*y^k[2],k=ST));
K := numer(1 - t*S):
I1,J1:=CalcInvRat(ST,x);
simplify(normal((I1-J1)/K));

#%% G3r
ST:={[-1,-1],[0,1],[1,0],[1,-1],[2,1]}:
S:=subs(add(x^k[1]*y^k[2],k=ST));
K := numer(1 - t*S):
I1,J1:=CalcInvRat(ST,x);
simplify(normal((I1-J1)/K));

#%% Pas de Gessel
Gessel:={[-1,0],[1,0],[1,1],[-1,-1]};
S:=subs(add(x^k[1]*y^k[2],k=ST));
K := numer(1 - t*S):
I1,J1:=CalcInvRat(Gessel,y);
simplify(normal((I1-J1)/K));

#%% Pas de Kreweras
Krew:={[-1,0],[0,-1],[1,1]};
S:=subs(add(x^k[1]*y^k[2],k=ST));
K := numer(1 - t*S):
I1,J1:=CalcInvRat(Krew,x);
simplify(normal((I1-J1)/K));
#%%

