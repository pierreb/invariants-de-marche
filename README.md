# Invariants de marche

Ici sont les feuilles de calcul Maple construisant des invariants pour certains modèles de marche

## Fichiers

La liste des fichiers :

* crat.mpl : construit les polynômes annulateurs de l’orbite si elle est finie et des invariants rationnels
(correspond à la partie 5.1)

* decouplage : ce dossier contient des procédures qui construisent les combinaisons de découplage
par rapport à certains modèles, puis l’appliquent au découplage de xy. Notamment, auto.mpl
est un début d’automatisation de cette procédure pour une orbite quelconque, ici instanciée sur
une orbite de taille 18 (correspond à la partie 5.2)

* algbéricité : dans ce dossier, on construit à partir des invariants rationnels et du découplage
des invariants satisfaisant le lemme des invariants, puis on prouve l’algébricité des solutions
des équations à une variable catalytique (correspond à la partie 6)

Les noms des fichiers dans les dossiers correspondent aux modèles condsidérés (notamment G2 retourné (g2r) et 
G3 retourné (g3r)).
