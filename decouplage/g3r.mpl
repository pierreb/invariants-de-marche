# S2 retourné

restart;

#%%

ST:={[-1,-1],[0,1],[1,0],[1,-1],[2,1]}:
S:=add(x^k[1]*y^k[2], k = ST):
K:=numer(S-1/t):


#%%

# On trouve une bonne extension de l’orbite, puis on calcule ses éléments
Mua:=factor(resultant(subs({x=x,y=Y},K),subs({x=X,y=Y},K),Y))/(t^2*(X-x)^2);

alias(a=RootOf(Mua,X));
y0,y1:=solve(factor(subs({x=x,y=Y},K),a),Y);
x1,x2:=solve(factor(evala(subs({y=y0,x=X},K)/(X-x)),a),X);
y2:=solve(factor(evala(subs({x=x1,y=Y},K)/(Y-y0)),a),Y);
y3:=solve(factor(evala(subs({x=x2,y=Y},K)/(Y-y0)),a),Y);
x5,x3:=solve(factor(evala(subs({x=X,y=y2},K)/(X-x1)),a),X);
x4:=solve(factor(evala(subs({x=X,y=y1},K)/((X-x)*(X-x3))),a),X);
x0:=RootOf(factor(subs({x=X,y=y},K)),X);

# On évalue xy sur l’équation de décuplaeg
a2:=evala(x*y0+x1*y0+x2*y0);
b2:=evala(x*y1+x2*y3+x1*y2);

Gy0:=simplify(a2/4-b2/4);
F:=evala(x*y0-Gy0); # F = -1/4*(3*t*x^2-t-4*x)/(x^2+1)/t
G:=evala(subs({x=x0},x*y-F)); # G = -1/4*(y+4)/y

# On vérifie que F+G est bien un découplage de xy
simplify((F+G-x*y)/K);
