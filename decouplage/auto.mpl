restart; 

with(PolynomialTools);
with(Algebraic);
with(ListTools);

read("crat.mpl");

ST:={[-1,0], [-1,-1], [2,0], [2,1]};
S:=add(x^k[1]*y^k[2],k=ST);
K:=numer(S-1/t);

p1,p2,_:=PQList(ST);
mu1:=mul(p1);
mu2:=mul(p2);

#%%

# on évalue et indexe les éléments de l'orbite
Split(mu1*subs(Y=X,mu2),X,'L');

# coup de bol : on a trouvé un élément primitif
a:=RootOf(evala(Minpoly(L[1],X)),X,index=1);

# on calcule les parties gauche et droite de l’orbite
o1:=[solve(factor(mu1,a),X)];
o2:=[solve(factor(mu2,a),Y)];

getOrb:=proc(mu1,mu2)
    local u,v,s:
    s:=NULL;
    for u in o1 do
        for v in o2 do
            if evala(subs({x=u,y=v},S)-S) = 0 then
               s := s , [u,v]:
            fi:
        od:
    od:
    [s]
end:

orb:=map(simplify,getOrb(mu1,mu2));

#%%

# il s’agit maintenant de construire les orbites sous les actions
# de Gx et Gy

# pour cela, on indexe les éléments uniquement

BuildClass := proc(p,u)
    local f;
    return [seq(`if`(degree(f[1],u) > 0, f[1],NULL), f = factors(p)[2])];
end:

ClassIndex := proc(orb,c1,c2)
    local f, i,j:
    local ps, qs, p, q:

    ps := NULL; qs := NULL;

    for i from 1 to numelems(orb) do
        for j from 1 to numelems(c1) do
            if evala(subs({X=orb[i][1],t=1/S},factor(c1[j],a))) = 0 then
                ps := ps, j;
            fi:
        od:
    od:

    for i from 1 to numelems(orb) do
        for j from 1 to numelems(c2) do
            if evala(subs({Y=orb[i][2],t=1/S},factor(c2[j],a))) = 0 then
                qs := qs, j;
            fi:
        od:
    od:

    [ps],[qs];
end:

#%%

# on évalue en K(x,y0) = 0 et on factorise sur Q(t,x)
# pour avoir l’action de Gx

ogx_1:=BuildClass(resultant(K,mu1,y),X);
ogx_2:=BuildClass(resultant(K,mu2,y),Y);

p1,q1:=ClassIndex(orb,ogx_1,ogx_2);

groupClass := proc(p1,q1)
    local z, cx, cr, cc, i;
    z := seq(zip((u,v)->[u,v], p1, q1));
    cx := NULL;
    for cr in {z} do
        cc := NULL;
        for i from 1 to numelems([z]) do
            if z[i] = cr then
                cc := cc, i;
            fi:
        od:
        cx := cx, [cc];
    od:
    cx := [cx];
end;

ogx := groupClass(p1,q1);

# on évalue en K(x0,y) = 0 et on factorise sur Q(t,y)
# pour avoir l’action de Gy

ogy_1:=BuildClass(resultant(K,mu1,x),X);
ogy_2:=BuildClass(resultant(K,mu2,x),Y);

p2,q2:=ClassIndex(orb,ogy_1,ogy_2);

ogy := groupClass(p2, q2);

# on a à ce stade des surorbites pour Gx et Gy

#%% 

# il s’agit maintenant de trouver les cycles alternés
# ie: ceux qui annulent les sections
# on ne connaît pas a priori la forme de l’orbite,
# donc on va plutôt regrouper les éléments qui partagent
# une coordonnée commune ensemble, et dire que la somme de leurs poids
# doit faire zéro

groupEqCoords := proc(orb)
    local co1, o, co2, ps, i, qs, q, c, p;
    co1 := {seq(o[1], o = orb)};
    co2 := {seq(o[2], o = orb)};
    ps := NULL;
    for c in co1 do
        p := seq(`if`(orb[i][1] = c, i, NULL), i = 1 .. numelems(orb));
        ps := ps, [p];
    od:
    qs := NULL;
    for c in co2 do
        q := seq(`if`(orb[i][2] = c, i, NULL), i = 1 .. numelems(orb));
        qs := qs, [q];
    od:
    return ps, qs;
end:

nullSec:=groupEqCoords(orb);

#%% 

M:=Matrix([seq([seq(`if`(member(i, c), 1, 0), i = 1 .. numelems(orb))], c = nullSec)]);

V0:=NullSpace(M);

L0:=seq([seq(v)], v = V0);

#%%

# on résout tout

Lx := seq([seq(`if`(member(i,c), 1, 0), i = 1 .. numelems(orb))], c = ogx);

Ly := seq([seq(`if`(member(i,c), 1, 0), i = 1 .. numelems(orb))], c = ogy);

MLines := [Lx, Ly, L0];

M2:=Matrix(MLines)^%T;

# !

S:=NullSpace(M2);

xyInd:=Search([x,y],orb);

L:=LinearSolve(M2, Vector([seq(`if`(i = xyInd, 1, 0),i = 1 .. numelems(orb))]),free='t');

Lp:=subs({seq(u=0,u=indets(L, 'indexed'))},L);

#%%

sumOver := proc(h, ls)
    local i:
    simplify(add(seq(
        `if`(ls[i]=1,subs({x=orb[i][1],y=orb[i][2]},h),NULL), i = 1 .. numelems(ls)))):
end:

decoupl := proc(h)
    local i, a, b, c:
    a:=add(seq(Lp[i] * sumOver(h, Lx[i]), i = 1 .. numelems([Lx])));
    b:=add(seq(Lp[numelems([Lx])+i] * sumOver(h, Ly[i]), i = 1 .. numelems([Ly])));
    c:=add(seq(Lp[numelems([Lx])+numelems([Ly])+i] * sumOver(h, L0[i]), i = 1 .. numelems([L0])));
    evala(a), evala(b), simplify(evala(c));
end:

a,b,c:=decoupl(x*y);

f:=evala(subs(y=RootOf(K,y),a));
g:=evala(subs(x=RootOf(K,x),b));

simplify(normal((x*y-f-g)/K));

#%%



I1:=coeff(map(simplify,collect(expand(mul(ogx_1))/lcoeff(mul(ogx_1),X),X)),X^3);
J1:=coeff(map(simplify,collect(expand(mul(ogy_1))/lcoeff(mul(ogy_1),X),X)),X^3);
normal((I1-I2)/K);

